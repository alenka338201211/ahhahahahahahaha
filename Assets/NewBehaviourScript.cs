﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewBehaviourScript : MonoBehaviour
{
    public Image im;
    private Vector3 mousePos;
    private float speed = 1f;
    private bool f = false;
    public GameObject yabl;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            f = true;
            mousePos.z = im.transform.position.z;
            Debug.Log(mousePos.ToString());
            yabl.transform.position = new Vector3(mousePos.x, mousePos.y, mousePos.z);
            yabl.SetActive(true);
        }
    }
    private void FixedUpdate()
    {
        if (f)
        {
            if (mousePos != im.transform.position)
            {
                MoveObj();
            }
            else
            {
                f = false;
                yabl.SetActive(false);
            }
        }
    }
    void MoveObj()
    {
        im.transform.Rotate(new Vector3(0, 0, 10));
        im.transform.position = Vector3.MoveTowards(im.transform.position, mousePos, speed);
    }
}
